package cl.clave;

import org.apache.log4j.Logger;
import org.apache.commons.lang3.StringUtils;

/**
 * Hello world!
 *
 */
public class App {
	final static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {
		String msg = "clave.cl";
		App.runMe(msg);

		logger.info("Con SpringUtils Capitalizado: " + StringUtils.capitalize(msg));
		logger.info("Hello World!" + valorNumerico(55));
	}

	public static int valorNumerico(int valor) {
		return valor +5;
	}

	private static void runMe(String parameter) {

		if (logger.isDebugEnabled()) {
			logger.debug("This is debug : " + parameter);
		}

		if (logger.isInfoEnabled()) {
			logger.info("This is info : " + parameter);
		}

		logger.warn("This is warn : " + parameter);
		logger.error("This is error : " + parameter);
		logger.fatal("This is fatal : " + parameter);

	}
}
